import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
declare var $:any;

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {
  @Input() anchura: number;
  @Input() etiquetas: boolean;
  @Output() conseguirAutor = new EventEmitter();

  public autor: any;

  constructor() {
    this.autor = {
      nombre: "Anggye Larez",
      web: "https://angular.io/",
      github: "https://github.com/lareza-web"
    };  
   }

  ngOnInit() {
    $("#logo").click(function(e){
      e.preventDefault();
      $("header").css("background","green")
                 .css("height","50px");
    });  
    
    $('.galeria').bxSlider({
      mode: 'fade',
      captions: this.etiquetas,
      slideWidth: this.anchura
    });
  }

  lanzar(event){
    console.log(event);
    this.conseguirAutor.emit(this.autor);
  }

}
